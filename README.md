Pharmacy app for Hack4Med
=====================

Overview
-------

This app is aimed at tourists traveling abroad and needing some self-medication drugs for the most common illness/sickness, 
and helping them finding the correct drug abroad, and the nearest open chemist/drugstore

At the moment it uses just the list of Chemists in Piedmont/Italy, but this idea can be applied to each country that offers 
open data about chemists and drugs.

So:

1. List and search nearby chemists, hilighting the open ones, and the ones about to close
2. Search for active ingredients or commercial names of the most common drugs, using the names most used in the 
	country of origin of the user
3. From this search obtain the commercial name of the drug (or generic drag) available in Italy
4. Search for the drugs via the common sickness or illness they are mostly used to cure

The link between different drugs, in different countries, and sicknesses, is the active pricinple of course. 

Open Data
--------

The open data used is the list of Piedmontese chemists. This list unfortunately is not updated, and is missing an interesting data: 
the opening hours.

(http://www.dati.piemonte.it/catalogodati/dato/100731-farmacie-pubbliche-e-private.html)

The resulting dataset has been converted to SQLite via CoreData, and we added:
* fake opening hours (just to test the concept)
* correct chemist's coordinates, obtained by calling Google Forward geocoding APIs

Thus the resulting CoreData database is normalized to our app, including latitude/longitude and some sample opening hours.

Moreover we have integarted in the same database the so called "parafarmacie", i.e. pseudo-chemists that sell over-the-counter drugs 
in supermarkets, malls, etc.. 

Other data
---------

We have looked on line for a list of Italian active ingredients and the corresponding drugs. There are some CSV on line, not complete 
but they give a pretty clear idea.
In order to have a fully functional app we need to obtain corresponding lists for all the countries we want to include in the app. 
As a starting point, we should choose the european countries whose tourists are the main visitor in Piedmont and Turin. along with 
USA.
A second step could be integrating eastern countries like China and Japan.

Another set of data needed is a list of common diseases and the corresponding active principle that cures it.

Data verification
---------------

The list of sicknesses and active principles must be compiled and checked by qualified doctors and chemists. This is crucial 
to having a sound correspondance between drugs and seecknesses, according also (if any) to difference in usage in the various countries,
and local habits. 


The App project
-------------

It's a simple XCode project targeting both iPad and iPhone. The layout was studied for a smartphone.
Everything is self contained. The only actual dataset is the one containing chemists and parapharmacies. 
Other data such as Drugs, Sicknesses and ActiveIngredients are modeled via CoreData, but the list of such entities are inserted as
a simulation in `DataManager init`

Once the corresponding datasets are found, and sanitized, they need to be inserted into the CoreData model, for efficience sake.