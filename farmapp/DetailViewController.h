//
//  DetailViewController.h
//  farmapp
//
//  Created by Federico Ianne on 16/05/14.
//  Copyright (c) 2014 Iakta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "FarmacyViewController.h"
#import "Drug.h"
#import "Chemist.h"

@interface DetailViewController : FarmacyViewController  {
    Drug *_drug;
    Chemist *_chemist;
}

//@property(nonatomic,weak) IBOutlet MKMapView* map;
@property (strong, nonatomic) IBOutlet UIView *bottomView;
@property (strong, nonatomic) IBOutlet UILabel *lbTitle;
@property (strong, nonatomic) IBOutlet UILabel *lbSubtitle;
@property (strong, nonatomic) IBOutlet UILabel *lbDescription;
@property (strong, nonatomic) IBOutlet UIImageView *ivImage;

-(void)setDrug:(Drug *)drug;
-(void)setChemist:(Chemist *)chemist;

@end
