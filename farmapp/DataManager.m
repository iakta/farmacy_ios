//
//  DataManager.m
//  farmapp
//
//  Created by Walter Gamba on 16/05/14.
//  Copyright (c) 2014 Iakta. All rights reserved.
//

#import "DataManager.h"


@interface DataManager()

@property (nonatomic, strong) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@end

#define ACETIL_ACID_ID 9
#define ASPIRIN_ID 1

@implementation DataManager



+(DataManager*)instance
{
    static DataManager* sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[DataManager alloc] init];
        //        [sharedInstance initializeDatabase];
    });
    
    return sharedInstance;
}

-(id)init{
    self=[super init];
    
    
    //data is loaded by project CSV2CoreDataConverter which reads from variuos CSVs and text files and builds the core data graph..
    return self;
    
    NSArray *pa=[self lookForActiveIngredients ];
    if(pa.count<750){
        //cancella tutti
        for(ActiveIngredient *ai in pa)
            [self.managedObjectContext deleteObject:ai];
        [self.managedObjectContext save:nil];
        
    //convert
        NSString* fileRoot = [[NSBundle mainBundle]
                          pathForResource:@"PRINCIPIOATTIVO" ofType:@"txt"];
    // read everything from text
        NSString* fileContents = [NSString stringWithContentsOfFile:fileRoot
                              encoding:NSUTF8StringEncoding error:nil];
    
    // first, separate by new line
        NSArray* allLinedStrings =  [fileContents componentsSeparatedByCharactersInSet:  [NSCharacterSet newlineCharacterSet]];
        int idcnt=1;
        for(NSString *pa in  allLinedStrings){
            ActiveIngredient *ai=[NSEntityDescription insertNewObjectForEntityForName:@"ActiveIngredient"
                                                  inManagedObjectContext:self.managedObjectContext];
            
            ai.name=pa;
            ai.ingredient_id=[NSNumber numberWithInt:idcnt];
            
            idcnt++;
            
        }
        [self.managedObjectContext save:nil];
    }
    ActiveIngredient *acetilsailiciliAcid=[self searchActIngWithText:@"acetilsalicilico" andCountryId:1]; //ID 1
    ActiveIngredient *parcetamol=[self searchActIngWithText:@"paracetamolo" andCountryId:1];; //ID 2
    ActiveIngredient *ibuprofen=[self searchActIngWithText:@"ibuprofene" andCountryId:1];; //ID 3
    
    //look for aspirin
    NSArray *drugs=[self searchDrugOrActIngWithText:@"acetilsalicilico" andCountryId:1];
    NSLog(@"Drugs %@",drugs);
    
//    for(NSManagedObject *o in drugs){
//        if([o respondsToSelector:@selector(ingredient_id)]){
//            ActiveIngredient *_aitemp=(ActiveIngredient *)o;
//            if([_aitemp.ingredient_id intValue]==ACETIL_ACID_ID){
//                acetilsailiciliAcid=_aitemp;
//                break;
//            }
//        }
//    }
    if(acetilsailiciliAcid){
        //add aspirin
        if([acetilsailiciliAcid.ing_drug count]==0   ){
            //add aspirin
            Drug *aspirin=[acetilsailiciliAcid.ing_drug anyObject];
            if(!aspirin){
                aspirin=[NSEntityDescription insertNewObjectForEntityForName:@"Drug"
                                                        inManagedObjectContext:self.managedObjectContext];
                aspirin.drug_id=[NSNumber numberWithInt:ASPIRIN_ID];
                aspirin.name=@"Aspirina";
                aspirin.detail=@"Cures various sickness such as headache, minor body pains, light flu, fever..etc etc.... un analgesico (antidolorifico: riduce il dolore), antinfiammatorio ed antipiretico   (antifebbrile: riduce la febbre)";
            /*
             Aspirina si usa per la terapia sintomatica degli stati febbrili e delle sindromi influenzali e da
             raffreddamento e per il trattamento sintomatico di mal di testa e di denti, nevralgie, dolori mestruali,
             dolori reumatici e muscolari.
             */
            
                acetilsailiciliAcid.ing_drug=[NSSet setWithObject:aspirin];
            }
            //add Fever, Headache, Tooth pain, Cold..
            Sickness *cold=[NSEntityDescription insertNewObjectForEntityForName:@"Sickness"
                                                         inManagedObjectContext:self.managedObjectContext];
            cold.sickness_id=[NSNumber numberWithInt:1];
            cold.name=@"Cold";
            cold.detail=@"You are feeling cold even if it is not";
            
            Sickness *toothache=[NSEntityDescription insertNewObjectForEntityForName:@"Sickness"
                                                         inManagedObjectContext:self.managedObjectContext];
            toothache.sickness_id=[NSNumber numberWithInt:2];
            toothache.name=@"Toothache";
            toothache.detail=@"Teeth are aching, and maybe even gums";
            
            Sickness *headache=[NSEntityDescription insertNewObjectForEntityForName:@"Sickness"
                                                         inManagedObjectContext:self.managedObjectContext];
            headache.sickness_id=[NSNumber numberWithInt:3];
            headache.name=@"Headache";
            headache.detail=@"Your head is about to explode";
            
            Sickness *flu=[NSEntityDescription insertNewObjectForEntityForName:@"Sickness"
                                                             inManagedObjectContext:self.managedObjectContext];
            flu.sickness_id=[NSNumber numberWithInt:4];
            flu.name=@"Flu";
            flu.detail=@"You got a bad flu, you sneeze and your nose is a faucet ";
            
            
            acetilsailiciliAcid.ing_sickness=[NSSet setWithObjects:cold, toothache, headache, flu, nil];
            aspirin.drug_sickness=[NSSet setWithObjects:cold, toothache, headache, flu, nil];
            
            [self.managedObjectContext save:nil];
        }else{
            for(Drug *d in acetilsailiciliAcid.ing_drug){
                NSLog(@"Acetil -> drug %@",d);
            }
            for(Sickness *s in acetilsailiciliAcid.ing_sickness){
                NSLog(@"Acetil -> sick %@",s);
                for(ActiveIngredient *a in s .sickness_ing){
                    NSLog(@"Sickness -> ing %@",a);
                    
                }
                for(Drug *a in s .sickness_drug){
                    NSLog(@"Sickness -> drug %@",a);
                    
                }
            }
        }
    }
    
    
    //add fake Sickness
    NSArray *ds=[self lookForSicknesses];
    if(ds.count==0){
        //add some
    }
    return self;
}

#pragma mark - CoreData

/**
 Returns the managed object context for the application.
 If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
 */
- (NSManagedObjectContext *)managedObjectContext {
    
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [NSManagedObjectContext new];
        [_managedObjectContext setPersistentStoreCoordinator: coordinator];
    }
    return _managedObjectContext;
}

/**
 Returns the managed object model for the application.
 If the model doesn't already exist, it is created by merging all of the models found in the application bundle.
 */
- (NSManagedObjectModel *)managedObjectModel {
    
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    _managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
    return _managedObjectModel;
}

/**
 Returns the URL to the application's documents directory.
 */
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

/**
 Returns the persistent store coordinator for the application.
 If the coordinator doesn't already exist, it is created and the application's store added to it.
 */
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // copy the default store (with a pre-populated data) into our Documents folder
    //
    NSString *documentsStorePath =
    [[[self applicationDocumentsDirectory] path] stringByAppendingPathComponent:@"Chemists.sqlite"];
    
    //if the expected store doesn't exist, copy the default store
    if (![[NSFileManager defaultManager] fileExistsAtPath:documentsStorePath]   ) {
        NSString *defaultStorePath = [[NSBundle mainBundle] pathForResource:@"Chemists" ofType:@"sqlite"];
        if (defaultStorePath) {
            [[NSFileManager defaultManager] copyItemAtPath:defaultStorePath toPath:documentsStorePath error:NULL];
        }
    }
    
    _persistentStoreCoordinator =
    [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    
    // add the default store to our coordinator
    NSError *error;
    NSURL *defaultStoreURL = [NSURL fileURLWithPath:documentsStorePath];
    NSLog(@"SQLITE path is %@",documentsStorePath);
    
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
                             [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
    
    
    
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
                                                   configuration:nil
                                                             URL:defaultStoreURL
                                                         options:options
                                                           error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. If it is not possible to recover from the error, display an alert panel that instructs the user to quit the application by pressing the Home button.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible
         * The schema for the persistent store is incompatible with current managed object model
         Check the error message to determine what the actual problem was.
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    
    
    return _persistentStoreCoordinator;
}



-(Chemist *)lookForChemistWithId:(NSNumber *)chemistId{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSManagedObjectContext *context=self.managedObjectContext;
    
    [request setEntity:[NSEntityDescription entityForName:@"Chemist" inManagedObjectContext:context ]];
    NSPredicate *where= [NSPredicate predicateWithFormat:@"(id_row = %@) ",chemistId];
    [request setPredicate:where];
    
    NSError *error=nil;
    NSMutableArray *mutableFetchResults = [[ context
                                            executeFetchRequest:request error:&error] mutableCopy];
    if (mutableFetchResults == nil || [mutableFetchResults count]==0) {
        // Handle the error.
        //        NSLog(@"Non ci sono Farmacie id %@", chemistId);
        return nil;
    }
    
    if( [mutableFetchResults count] == 1 ){
        return (Chemist *)[mutableFetchResults objectAtIndex:0];
    }else{
        NSLog(@"###### BUG: trovato piu di un Secret con lo stesso ID: %@", chemistId);
        //per ora ritorno il primo ma controllare!!
        return (Chemist *)[mutableFetchResults objectAtIndex:0];
    }
    
}

-(NSArray *)lookForChemists{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSManagedObjectContext *context=self.managedObjectContext;
    
    [request setEntity:[NSEntityDescription entityForName:@"Chemist" inManagedObjectContext:context ]];
    //    NSPredicate *where= [NSPredicate predicateWithFormat:@"(id_row = %@) ",chemistId];
    //    [request setPredicate:where];
    //
    NSError *error=nil;
    NSMutableArray *mutableFetchResults = [[ context
                                            executeFetchRequest:request error:&error] mutableCopy];
    if (mutableFetchResults == nil || [mutableFetchResults count]==0) {
        // Handle the error.
        NSLog(@"Non ci sono Farmacie ");
        return nil;
    }
    
    return mutableFetchResults;
}

-(NSArray *)lookForActiveIngredients{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSManagedObjectContext *context=self.managedObjectContext;
    
    [request setEntity:[NSEntityDescription entityForName:@"ActiveIngredient" inManagedObjectContext:context ]];
    //
    NSError *error=nil;
    NSMutableArray *mutableFetchResults = [[ context
                                            executeFetchRequest:request error:&error] mutableCopy];
    if (mutableFetchResults == nil || [mutableFetchResults count]==0) {
        // Handle the error.
        NSLog(@"Non ci sono Pattivi ");
        return nil;
    }
    
    return mutableFetchResults;
}

-(NSArray *)lookForSicknesses{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSManagedObjectContext *context=self.managedObjectContext;
    
    [request setEntity:[NSEntityDescription entityForName:@"Sickness" inManagedObjectContext:context ]];
    //
    NSPredicate *where= [NSPredicate predicateWithFormat:@"(name != NULL) "];
      [request setPredicate:where];
    NSError *error=nil;
    NSMutableArray *mutableFetchResults = [[ context
                                            executeFetchRequest:request error:&error] mutableCopy];
    if (mutableFetchResults == nil || [mutableFetchResults count]==0) {
        // Handle the error.
        NSLog(@"Non ci sono Sickness ");
        return nil;
    }
    
    return mutableFetchResults;
}

-(NSArray *)lookForDrugs{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSManagedObjectContext *context=self.managedObjectContext;
    
    [request setEntity:[NSEntityDescription entityForName:@"Drug" inManagedObjectContext:context ]];
    //
    NSError *error=nil;
    NSMutableArray *mutableFetchResults = [[ context
                                            executeFetchRequest:request error:&error] mutableCopy];
    if (mutableFetchResults == nil || [mutableFetchResults count]==0) {
        // Handle the error.
        NSLog(@"Non ci sono Drug ");
        return nil;
    }
    
    return mutableFetchResults;
}


-(NSArray *)searchSicknessWithText:(NSString *)str andCountryId:(int)countryId{
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSManagedObjectContext *context=self.managedObjectContext;
    
    [request setEntity:[NSEntityDescription entityForName:@"Sickness" inManagedObjectContext:context ]];
    
    if(str==nil || str.length==0)
        return nil;
    
    //now add predicates
    NSString *whereStr=[NSString stringWithFormat: @"(name contains[cd] \"%@\") or (symptoms contains[cd] \"%@\") or (detail contains[cd] \"%@\")",str,str,str];
    [request setPredicate:[NSPredicate predicateWithFormat:whereStr]];
    
    NSError *error=nil;
    NSMutableArray *mutableFetchResults = [[ context
                                            executeFetchRequest:request error:&error] mutableCopy];
    if (mutableFetchResults == nil || [mutableFetchResults count]==0) {
        return nil;
    }
    return  mutableFetchResults;

}
-(NSArray *)searchDrugOrActIngWithText:(NSString *)str andCountryId:(int)countryId{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSManagedObjectContext *context=self.managedObjectContext;
    if(str==nil || str.length==0)
        return nil;
    
    //search for DRUGS
    [request setEntity:[NSEntityDescription entityForName:@"Drug" inManagedObjectContext:context ]];
    
    //now add predicates
    NSString *whereStr=[NSString stringWithFormat: @"(name contains[cd] \"%@\") or (detail contains[cd] \"%@\")",str,str];
    [request setPredicate:[NSPredicate predicateWithFormat:whereStr]];
    
    //TODO add filter for Country ID
    
    NSError *error=nil;
    NSMutableArray *resultsDrugs = [[ context   executeFetchRequest:request error:&error] mutableCopy];
    if (resultsDrugs == nil || [resultsDrugs count]==0) {
        resultsDrugs =[NSMutableArray array];//empty
    }
    
    //now look for ActiveIngredient
    //search for DRUGS
    [request setEntity:[NSEntityDescription entityForName:@"ActiveIngredient" inManagedObjectContext:context ]];
    
    //now add predicates
    whereStr=[NSString stringWithFormat: @"(name contains[cd] \"%@\") or (detail contains[cd] \"%@\")",str,str];
    [request setPredicate:[NSPredicate predicateWithFormat:whereStr]];
    
    //TODO add filter for Country ID
    
    NSMutableArray *resultsActiveIng = [[ context   executeFetchRequest:request error:&error] mutableCopy];
    if (resultsActiveIng == nil || [resultsActiveIng count]==0) {
        resultsActiveIng =[NSMutableArray array];//empty
    }
    
    //make a union
    [resultsDrugs addObjectsFromArray:resultsActiveIng];
    
    //
    return resultsDrugs;
}

-(ActiveIngredient *)searchActIngWithText:(NSString *)str andCountryId:(int)countryId{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSManagedObjectContext *context=self.managedObjectContext;
    if(str==nil || str.length==0)
        return nil;
   
    //now look for ActiveIngredient
    //search for DRUGS
    [request setEntity:[NSEntityDescription entityForName:@"ActiveIngredient" inManagedObjectContext:context ]];
    
    //now add predicates
    NSString *whereStr=[NSString stringWithFormat: @"(name contains[cd] \"%@\") or (detail contains[cd] \"%@\")",str,str];
    [request setPredicate:[NSPredicate predicateWithFormat:whereStr]];
    
    //TODO add filter for Country ID
    NSError *error=nil;
    
    NSMutableArray *resultsActiveIng = [[ context   executeFetchRequest:request error:&error] mutableCopy];
    if (resultsActiveIng == nil || [resultsActiveIng count]==0) {
        return nil;
    }
    
    return [resultsActiveIng objectAtIndex:0];
}


-(NSArray *)searchChemistsWithName:(NSString *)name andAddress:(NSString *)address andCoordinate:(CLLocationCoordinate2D)coord withinRange:(float)kms{    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSManagedObjectContext *context=self.managedObjectContext;
    
    [request setEntity:[NSEntityDescription entityForName:@"Chemist" inManagedObjectContext:context ]];
    
    //now add predicates 
    NSString *whereStr=nil;
    if(name!=nil){
        if(!whereStr)
            whereStr=@"";
        else
            whereStr=[whereStr stringByAppendingString:@" and "];
        whereStr=[whereStr stringByAppendingFormat:@"(denominazione contains[cd] \"%@\")",name];
    }
    if(address!=nil){
        if(!whereStr)
            whereStr=@"";
        else
            whereStr=[whereStr stringByAppendingString:@" and "];
        whereStr=[whereStr stringByAppendingFormat:@"(descrizione_indirizzo contains[cd] \"%@\")",address];
    }
    if(kms>0.001){
        //convert from kms to degrees
        float degSpan=kms/111.12; //rough
        float latA=coord.latitude-degSpan;
        float latB=coord.latitude+degSpan;
        
        float lngA=coord.longitude-degSpan;
        float lngB=coord.longitude+degSpan;
        
        if(!whereStr)
            whereStr=@"";
        else
            whereStr=[whereStr stringByAppendingString:@" and "];
        whereStr=[whereStr stringByAppendingFormat:@"(latitudine >= %f and latitudine <=%f and longitudine >=%f and longitudine <= %f)",latA,latB,lngA,lngB];
    }
//    if(true ){
//        if(!whereStr)
//            whereStr=@"";
//        else
//            whereStr=[whereStr stringByAppendingString:@" and "];
//        
//        whereStr=[whereStr stringByAppendingFormat:@"(is_parafarmacia == 0)"];
//    }
    [request setPredicate:[NSPredicate predicateWithFormat:whereStr]];
    
    NSError *error=nil;
    NSMutableArray *mutableFetchResults = [[ context
                                            executeFetchRequest:request error:&error] mutableCopy];
    if (mutableFetchResults == nil || [mutableFetchResults count]==0) {
        return nil;
    }
     return  mutableFetchResults   ;
} 

@end
