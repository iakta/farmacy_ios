//
//  ViewController.h
//  farmapp
//
//  Created by Federico Ianne on 15/05/14.
//  Copyright (c) 2014 Iakta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "FarmacyViewController.h"

@interface HomeViewController : FarmacyViewController  <UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate>{
}

//@property(nonatomic,weak) IBOutlet MKMapView* map;
@property (strong, nonatomic) IBOutlet UIView *bottomView;
@property (strong, nonatomic) IBOutlet UIButton *btPharmacy;
@property (strong, nonatomic) IBOutlet UIButton *btMedicine;
@property (strong, nonatomic) IBOutlet UIButton *btDiseases;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property(nonatomic,weak) IBOutlet UITableView *tableView;

- (IBAction)actionPharmacy;
- (IBAction)actionMedicine;
- (IBAction)actionDiseases;


@end
