#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>

#define RGB(r, g, b) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1]
#define RGBA(r, g, b, a) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:(a)]

#define FONT_SIZE_LARGE     30
#define FONT_SIZE_NORMAL    22
#define FONT_SIZE_SMALL     17

static UIBarButtonItem* backButton(id owner)
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    
    button.frame = CGRectMake(0,0,44,44);
    button.imageEdgeInsets=UIEdgeInsetsMake(14,0,15,26);; //t l b r
    [button setImage:[UIImage imageNamed:@"icona_back.png"] forState:UIControlStateNormal];
    [button setTitle:@"back" forState:UIControlStateNormal];
    [button addTarget:owner action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    return [[UIBarButtonItem alloc] initWithCustomView:button];
}



#pragma mark - UIColor utilities
@interface UIColor(Utilities)
+(UIColor*)lightGreenColor;
+(UIColor*)darkGreenColor;
+(UIColor*)grayAppColor;
@end
@implementation UIColor(Utilities)

+(UIColor*)lightGreenColor{ return RGB(181,221,44); }
+(UIColor*)darkGreenColor{ return RGB(125,160,3); }
+(UIColor*)grayAppColor{ return RGB(178,178,178); }


@end


#pragma mark UIFont Utilities

@interface UIFont(Utilities)
+(UIFont*)appFontOfSize:(CGFloat)fontSize;
@end


@implementation UIFont(Utilities)
+(UIFont*)appFontOfSize:(CGFloat)fontSize{ return [UIFont fontWithName:@"Helvetica-Light" size:fontSize]; }
@end

#pragma mark - settings


static NSString* loadSettingsWithKey(NSString* key);

static NSDate* loadDateSettingsWithKey(NSString* key);
static void saveDateSettingWithKey(NSDate *date,NSString *key);

static void saveSettingWithKey(NSString* value, NSString* key);
static NSNumber* loadNumberSettingsWithKey(NSString* key);
static void saveNumberSettingWithKey(NSNumber* value, NSString* key);

static void saveDictSettingWithKey(NSDictionary* value, NSString* key);

static NSDictionary *loadDictSettingWithKey(NSString* key);
