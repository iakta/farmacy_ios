//
//  SettingsViewController.h
//  farmapp
//
//  Created by Federico Ianne on 16/05/14.
//  Copyright (c) 2014 Iakta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsViewController : UIViewController <UIPickerViewDelegate, UIPickerViewDataSource>

@property (strong, nonatomic) IBOutlet UILabel *lbInfo;
@property (strong, nonatomic) IBOutlet UILabel *lbLanguage;
@property (strong, nonatomic) IBOutlet UIPickerView *pickerLanguage;
@property (strong, nonatomic) IBOutlet UIButton *btConfirm;
@property (strong, nonatomic) NSArray *languages;

- (IBAction)actionConfirm;

@end
