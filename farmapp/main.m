//
//  main.m
//  farmapp
//
//  Created by Federico Ianne on 15/05/14.
//  Copyright (c) 2014 Iakta. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
