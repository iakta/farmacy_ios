//
//  Chemist.h
//  farmapp
//
//  Created by Walter Gamba on 17/05/14.
//  Copyright (c) 2014 Iakta. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import <MapKit/MapKit.h>

typedef enum : NSUInteger {
    OpeningStatusUndefined=0,
    OpeningStatusOpen,
    OpeningStatusClosing,
    OpeningStatusClosed
} OpeningStatus;

@interface Chemist : NSManagedObject <MKAnnotation>

@property (nonatomic, retain) NSString * classificazione;
@property (nonatomic, retain) NSString * cod_farmacia;
@property (nonatomic, retain) NSString * data_inizio_att;
@property (nonatomic, retain) NSString * denominazione;
@property (nonatomic, retain) NSString * denominazione_comune;
@property (nonatomic, retain) NSString * denominazione_sede;
@property (nonatomic, retain) NSString * descrizione_indirizzo;
@property (nonatomic, retain) NSString * distretto;
@property (nonatomic, retain) NSString * id_azienda;
@property (nonatomic, retain) NSString * id_distretto;
@property (nonatomic, retain) NSString * id_farmacia;
@property (nonatomic, retain) NSNumber * id_row;
@property (nonatomic, retain) NSNumber * is_parafarmacia;
@property (nonatomic, retain) NSString * latitudine;
@property (nonatomic, retain) NSString * longitudine;
@property (nonatomic, retain) NSString * orario;

@property (nonatomic, readonly) BOOL isParafarmacia;


@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;


// Title and subtitle for use by selection UI.
@property (nonatomic, readonly, copy) NSString *title;
@property (nonatomic, readonly, copy) NSString *subtitle;

-(OpeningStatus)getOpeningStatus;

@end
