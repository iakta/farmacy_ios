//
//  ActiveIngredient.h
//  farmapp
//
//  Created by Walter Gamba on 18/05/14.
//  Copyright (c) 2014 Iakta. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Drug, Sickness;

@interface ActiveIngredient : NSManagedObject

@property (nonatomic, retain) NSString * detail;
@property (nonatomic, retain) NSNumber * ingredient_id;
@property (nonatomic, retain) NSNumber * language_id;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * usage;
@property (nonatomic, retain) NSSet *ing_drug;
@property (nonatomic, retain) NSSet *ing_sickness;
@end

@interface ActiveIngredient (CoreDataGeneratedAccessors)

- (void)addIng_drugObject:(Drug *)value;
- (void)removeIng_drugObject:(Drug *)value;
- (void)addIng_drug:(NSSet *)values;
- (void)removeIng_drug:(NSSet *)values;

- (void)addIng_sicknessObject:(Sickness *)value;
- (void)removeIng_sicknessObject:(Sickness *)value;
- (void)addIng_sickness:(NSSet *)values;
- (void)removeIng_sickness:(NSSet *)values;

@end
