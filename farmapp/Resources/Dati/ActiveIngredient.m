//
//  ActiveIngredient.m
//  farmapp
//
//  Created by Walter Gamba on 18/05/14.
//  Copyright (c) 2014 Iakta. All rights reserved.
//

#import "ActiveIngredient.h"
#import "Drug.h"
#import "Sickness.h"


@implementation ActiveIngredient

@dynamic detail;
@dynamic ingredient_id;
@dynamic language_id;
@dynamic name;
@dynamic usage;
@dynamic ing_drug;
@dynamic ing_sickness;

-(NSString *)description{
    return [NSString stringWithFormat:@"ActiveIngredient[%d,%d]: %@, %@, %@",[self.ingredient_id intValue],[self.language_id intValue], self.name, self.detail, self.usage];
}
@end
