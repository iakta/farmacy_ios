//
//  Sickness.m
//  farmapp
//
//  Created by Walter Gamba on 18/05/14.
//  Copyright (c) 2014 Iakta. All rights reserved.
//

#import "Sickness.h"
#import "ActiveIngredient.h"
#import "Drug.h"


@implementation Sickness

@dynamic detail;
@dynamic icon;
@dynamic language_id;
@dynamic name;
@dynamic sickness_id;
@dynamic symptoms;
@dynamic sickness_drug;
@dynamic sickness_ing;
@dynamic image;

-(NSString *)description{
    return [NSString stringWithFormat:@"Sickness[%d,%d]: %@, %@, %@",[self.sickness_id intValue],[self.language_id intValue], self.name, self.detail, self.symptoms];
}
@end
