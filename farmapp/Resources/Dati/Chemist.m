//
//  Chemist.m
//  farmapp
//
//  Created by Walter Gamba on 17/05/14.
//  Copyright (c) 2014 Iakta. All rights reserved.
//

#import "Chemist.h"


@implementation Chemist

@dynamic classificazione;
@dynamic cod_farmacia;
@dynamic data_inizio_att;
@dynamic denominazione;
@dynamic denominazione_comune;
@dynamic denominazione_sede;
@dynamic descrizione_indirizzo;
@dynamic distretto;
@dynamic id_azienda;
@dynamic id_distretto;
@dynamic id_farmacia;
@dynamic id_row;
@dynamic is_parafarmacia;
@dynamic latitudine;
@dynamic longitudine;
@dynamic orario;


-(BOOL)isParafarmacia{
    return [self.is_parafarmacia boolValue];
}
-(NSString *)title{
    return self.denominazione;
}
-(NSString *)subtitle{
    return self.descrizione_indirizzo;
}

-(CLLocationCoordinate2D)coordinate{
    return CLLocationCoordinate2DMake([self.latitudine doubleValue], [self.longitudine doubleValue]);
}

-(OpeningStatus)getOpeningStatus{
    //parse "orario".. that is                 chemist.orario=@"9:00-22:00";
    NSArray *hours=[self.orario componentsSeparatedByString:@"-"];
    if(hours.count!=2)
        return OpeningStatusUndefined;
    
    NSString *from=hours[0] ;
    NSString *to=hours[1] ;
    
    
    //split compo
    NSDate *opening=[self getTimeFromString:from];
    NSDate *closing=[self getTimeFromString:to];
    NSDate *halfAnHourBeforeClosing=[NSDate dateWithTimeInterval:-30*60 sinceDate:closing];
    //after opening
    if([opening timeIntervalSinceNow] < 0  && [closing timeIntervalSinceNow]>0){
        //but before half an hour of closing
        if([halfAnHourBeforeClosing timeIntervalSinceNow]>0)
            return OpeningStatusOpen;
        return OpeningStatusClosing;
    }
    
    return OpeningStatusClosed;
}

-(NSDate *)getTimeFromString:(NSString *)hour{
    NSArray *hours=[hour  componentsSeparatedByString:@":"];

    if(hours.count!=2)
        return nil;
    
    NSString *hr=hours[0] ;
    NSString *mn=hours[1] ;

    return [self getTodaysHour:[hr intValue] minutes:[mn intValue]];
}

-(NSDate *)getTodaysHour:(int)hour minutes:(int)minutes{
    // Use the user's current calendar and time zone
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSTimeZone *timeZone = [NSTimeZone systemTimeZone];
    [calendar setTimeZone:timeZone];
    NSDate *now=[NSDate date];

    // Selectively convert the date components (year, month, day) of the input date
    NSDateComponents *dateComps = [calendar components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit fromDate:now];
    
    // Set the time components manually
    [dateComps setHour:hour];
    [dateComps setMinute:minutes];
    [dateComps setSecond:0];
    
    // Convert back
    NSDate *beginningOfDay = [calendar dateFromComponents:dateComps];
    return beginningOfDay;
}

@end
