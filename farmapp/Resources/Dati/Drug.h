//
//  Drug.h
//  farmapp
//
//  Created by Walter Gamba on 18/05/14.
//  Copyright (c) 2014 Iakta. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class ActiveIngredient, Sickness;

@interface Drug : NSManagedObject

@property (nonatomic, retain) NSNumber * country_id;
@property (nonatomic, retain) NSString * detail;
@property (nonatomic, retain) NSNumber * drug_id;
@property (nonatomic, retain) NSString * effects;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSSet *drug_sickness;
@property (nonatomic, retain) ActiveIngredient *drug_ing;
@end

@interface Drug (CoreDataGeneratedAccessors)

- (void)addDrug_sicknessObject:(Sickness *)value;
- (void)removeDrug_sicknessObject:(Sickness *)value;
- (void)addDrug_sickness:(NSSet *)values;
- (void)removeDrug_sickness:(NSSet *)values;

@end
