//
//  Drug.m
//  farmapp
//
//  Created by Walter Gamba on 18/05/14.
//  Copyright (c) 2014 Iakta. All rights reserved.
//

#import "Drug.h"
#import "ActiveIngredient.h"
#import "Sickness.h"


@implementation Drug

@dynamic country_id;
@dynamic detail;
@dynamic drug_id;
@dynamic effects;
@dynamic name;
@dynamic drug_sickness;
@dynamic drug_ing;
-(NSString *)description{
    return [NSString stringWithFormat:@"Drug[%d,%d]: %@, %@, %@",[self.drug_id intValue],[self.country_id intValue], self.name, self.detail, self.effects];
}


@end
