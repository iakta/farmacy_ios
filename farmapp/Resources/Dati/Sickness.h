//
//  Sickness.h
//  farmapp
//
//  Created by Walter Gamba on 18/05/14.
//  Copyright (c) 2014 Iakta. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class ActiveIngredient, Drug;

@interface Sickness : NSManagedObject

@property (nonatomic, retain) NSData * image;
@property (nonatomic, retain) NSString * detail;
@property (nonatomic, retain) NSDate * icon;
@property (nonatomic, retain) NSNumber * language_id;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * sickness_id;
@property (nonatomic, retain) NSString * symptoms;
@property (nonatomic, retain) NSSet *sickness_drug;
@property (nonatomic, retain) NSSet *sickness_ing;
@end

@interface Sickness (CoreDataGeneratedAccessors)

- (void)addSickness_drugObject:(Drug *)value;
- (void)removeSickness_drugObject:(Drug *)value;
- (void)addSickness_drug:(NSSet *)values;
- (void)removeSickness_drug:(NSSet *)values;

- (void)addSickness_ingObject:(ActiveIngredient *)value;
- (void)removeSickness_ingObject:(ActiveIngredient *)value;
- (void)addSickness_ing:(NSSet *)values;
- (void)removeSickness_ing:(NSSet *)values;

@end
