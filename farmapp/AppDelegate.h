//
//  AppDelegate.h
//  farmapp
//
//  Created by Federico Ianne on 15/05/14.
//  Copyright (c) 2014 Iakta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
