#include "DefUtilities.h"
#import <UIKit/UIKit.h>




static NSString* loadSettingsWithKey(NSString* key)
{
    NSUserDefaults* defSettings = [NSUserDefaults standardUserDefaults];
    return [defSettings objectForKey:key];
}

static NSDate* loadDateSettingsWithKey(NSString* key)
{
    NSUserDefaults* defSettings = [NSUserDefaults standardUserDefaults];
    double d=[defSettings doubleForKey:key];
    return [NSDate dateWithTimeIntervalSince1970:d];
}

static void saveDateSettingWithKey(NSDate *date,NSString *key){
    double d=[date timeIntervalSince1970];
    
    NSUserDefaults* defSettings = [NSUserDefaults standardUserDefaults];
    [defSettings setDouble:d  forKey:key];
    [defSettings synchronize];
}

static void saveSettingWithKey(NSString* value, NSString* key)
{
    NSUserDefaults* defSettings = [NSUserDefaults standardUserDefaults];
    [defSettings setObject:value forKey:key];
    [defSettings synchronize];
}
static NSNumber* loadNumberSettingsWithKey(NSString* key)
{
    NSUserDefaults* defSettings = [NSUserDefaults standardUserDefaults];
    
    return [defSettings objectForKey:key];
}

static void saveNumberSettingWithKey(NSNumber* value, NSString* key)
{
    NSUserDefaults* defSettings = [NSUserDefaults standardUserDefaults];
    [defSettings setObject:value forKey:key];
    [defSettings synchronize];
}

static void saveDictSettingWithKey(NSDictionary* value, NSString* key)
{
    NSUserDefaults* defSettings = [NSUserDefaults standardUserDefaults];
    [defSettings setObject:value forKey:key];
    [defSettings synchronize];
}

static NSDictionary *loadDictSettingWithKey(NSString* key)
{
    NSUserDefaults* defSettings = [NSUserDefaults standardUserDefaults];
    
    return [defSettings objectForKey:key];
}
