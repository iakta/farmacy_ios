//
//  FarmacyViewController.h
//  farmapp
//
//  Created by Walter Gamba on 17/05/14.
//  Copyright (c) 2014 Iakta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>


@interface FarmacyViewController : UIViewController<MKMapViewDelegate>{
    NSArray *chemistList;
}
@property(nonatomic,weak) IBOutlet MKMapView* map;

@end
