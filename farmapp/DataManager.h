//
//  DataManager.h
//  farmapp
//
//  Created by Walter Gamba on 16/05/14.
//  Copyright (c) 2014 Iakta. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

#import "Chemist.h" 
#import "ActiveIngredient.h"
#import "Drug.h"
#import "Sickness.h"

@interface DataManager : NSObject{

}
+(DataManager*)instance;
//- (NSManagedObjectContext *)managedObjectContext ;
-(Chemist *)lookForChemistWithId:(NSNumber *)chemistId;
-(NSArray *)lookForChemists;
-(NSArray *)searchChemistsWithName:(NSString *)name andAddress:(NSString *)address andCoordinate:(CLLocationCoordinate2D)coord withinRange:(float)kms;


-(NSArray *)lookForActiveIngredients;
-(NSArray *)lookForSicknesses;
-(NSArray *)lookForDrugs;

-(NSArray *)searchSicknessWithText:(NSString *)str andCountryId:(int)countryId;
-(NSArray *)searchDrugOrActIngWithText:(NSString *)str andCountryId:(int)countryId;
-(ActiveIngredient *)searchActIngWithText:(NSString *)str andCountryId:(int)countryId;



//test
//-(void)convertFromStringLatToDoubleLat;
@end
