//
//  SettingsViewController.m
//  farmapp
//
//  Created by Federico Ianne on 16/05/14.
//  Copyright (c) 2014 Iakta. All rights reserved.
//

#import "SettingsViewController.h"
#import "DefUtilities.h"
#import "DataManager.h"

@interface SettingsViewController (){
    UIImageView *ivLogo;
}

@end

@implementation SettingsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.languages = @[@"Italy", @"Germany", @"Switzerland",@"Spain",
                      @"France", @"Great Britain", @"Japan", @"China"];
    
    self.lbInfo.font = [UIFont appFontOfSize:FONT_SIZE_SMALL];
    self.lbLanguage.font = [UIFont appFontOfSize:FONT_SIZE_NORMAL];
    self.btConfirm.titleLabel.font = [UIFont appFontOfSize:FONT_SIZE_LARGE];
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    // add logo
//    UIImageView *iv = [[UIImageView alloc] initWithFrame:CGRectMake(160, 60
//                                                                    , 0, 0)];
    ivLogo= [[UIImageView alloc] initWithFrame:CGRectMake(160, 1136/4
                                                                    , 0, 0)];//145x50
    ivLogo.image = [UIImage imageNamed:@"logo.png"];
    ivLogo.contentMode = UIViewContentModeCenter;
    [self.view addSubview:ivLogo];


}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)actionConfirm {
    // save language setting
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //hide everything..
    self.lbInfo.hidden=NO;
    self.lbLanguage.hidden=NO;
    self.pickerLanguage.hidden=NO;
    self.btConfirm.hidden=NO;
    
    self.lbInfo.alpha=0;
    self.lbLanguage.alpha=0;
    self.pickerLanguage.alpha=0;
    self.btConfirm.alpha=0;
    
    [UIView animateWithDuration:1.0 animations:^{
        self.lbInfo.alpha=1;
        self.lbLanguage.alpha=1;
        self.pickerLanguage.alpha=1;
        self.btConfirm.alpha=1;
        
        ivLogo.frame=CGRectMake(160, 60, 0, 0);
        
    }];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    NSArray *list=[[DataManager instance] lookForChemists];
    NSLog(@"Found %d chemists",list.count);
}

#pragma mark PickerView DataSource

- (NSInteger)numberOfComponentsInPickerView: (UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return self.languages.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return self.languages[row];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
