//
//  DetailViewController.m
//  farmapp
//
//  Created by Federico Ianne on 16/05/14.
//  Copyright (c) 2014 Iakta. All rights reserved.
//

#import "DetailViewController.h"
#import "DefUtilities.h"
#import "DataManager.h"

@interface DetailViewController ()

@end

@implementation DetailViewController


-(void)setDrug:(Drug *)drug{
    _drug=drug;
}
-(void)setChemist:(Chemist *)chemist{
    _chemist=chemist;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if(_drug){
        self.lbTitle.text=_drug.name;
        self.lbSubtitle.text=_drug.detail;
        self.lbDescription.text=_drug.effects;
        //
        self.map.region=MKCoordinateRegionMake(CLLocationCoordinate2DMake(45.066, 7.7),  MKCoordinateSpanMake(0.01, 0.01));
        MKUserLocation *ul=[[MKUserLocation alloc]init];
        ul.coordinate=CLLocationCoordinate2DMake(45.066, 7.7);
        [self mapView:self.map didUpdateUserLocation:ul ];

    }
    else if(_chemist){
        self.lbTitle.text=_chemist.denominazione;
        self.lbSubtitle.text=_chemist.descrizione_indirizzo;
        self.lbDescription.text=_chemist.orario;
        //center map..
        self.map.region=MKCoordinateRegionMake(_chemist.coordinate,  MKCoordinateSpanMake(0.005, 0.005));
        NSArray *chemists=[NSArray arrayWithObject:_chemist];
        [self.map showAnnotations:chemists animated:NO];
    }
}
#define SEARCH_RADIUS_KM 0.5

//- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation{
//    // [self.map setCenterCoordinate: userLocation.coordinate animated:YES];
//    //search
//    NSArray *chemists=[[DataManager instance] searchChemistsWithName:nil andAddress:nil andCoordinate:userLocation.coordinate withinRange:SEARCH_RADIUS_KM];
//    NSLog(@"Found %d chemists in radius %f km",chemists.count, SEARCH_RADIUS_KM);
//    
//    
//    [self.map showAnnotations:chemists animated:NO];
//    chemistList=chemists;
//    [self.tableView reloadData];
//}

/*
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id < MKAnnotation >)annotation{
    static NSString *defaultPinID = @"com.invasivecode.pin";
    MKAnnotationView *av= (MKAnnotationView *)[self.map dequeueReusableAnnotationViewWithIdentifier:defaultPinID];
    if ( av == nil )
        av = [[MKPinAnnotationView alloc]  initWithAnnotation:annotation reuseIdentifier:defaultPinID];
    
    Chemist *chemist=(Chemist *)annotation;
    
    OpeningStatus open= OpeningStatusUndefined;
    if([chemist respondsToSelector:@selector(getOpeningStatus)])
        open=[chemist getOpeningStatus];
    NSString *imageName=@"";
    switch(open){
        case OpeningStatusUndefined:
            imageName=@"puntatore";
            break;
        case OpeningStatusOpen:
            imageName=chemist.isParafarmacia ? @"punt_p_aperta" : @"punt_f_aperta";
            break;
        case OpeningStatusClosing:
            imageName=chemist.isParafarmacia ? @"punt_p_apertura" : @"punt_f_chiusura";
            break;
        case OpeningStatusClosed:
            imageName=chemist.isParafarmacia ? @"punt_p_chiusa" : @"punt_f_chiusa";
            break;
            
    }
    av.image=[UIImage imageNamed:imageName];
    CGRect frame=av.frame;
    frame.size=CGSizeMake(av.image.size.width/2, av.image.size.height/2);
    av.frame= frame;
    //    [annotationsArray addObject:av];
    return av;
}
 */


@end
