//
//  FarmacyViewController.m
//  farmapp
//
//  Created by Walter Gamba on 17/05/14.
//  Copyright (c) 2014 Iakta. All rights reserved.
//

#import "FarmacyViewController.h"
#import "DefUtilities.h"
#import "DataManager.h"

#define SEARCH_RADIUS_KM 0.5

@interface FarmacyViewController ()

@end

@implementation FarmacyViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.navigationItem setLeftBarButtonItem:backButton(self)];
    UIImageView *iv=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"logo"]];
//    iv.frame=CGRectMake(0, 0, 145, 50);
    self.navigationItem.titleView=iv;
  
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden=NO;
    UINavigationBar *nb=self.navigationController.navigationBar;
    self.navigationController.navigationBar.translucent=YES;
    self.navigationController.navigationBar.alpha=0.5;
//    nb.backgroundColor=[UIColor colorWithWhite:0.5 alpha:0.5];
}


-(void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
 


#pragma mark - map delegate

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation{
    // [self.map setCenterCoordinate: userLocation.coordinate animated:YES];
    //search
    NSArray *chemists=[[DataManager instance] searchChemistsWithName:nil andAddress:nil andCoordinate:userLocation.coordinate withinRange:SEARCH_RADIUS_KM];
    NSLog(@"Found %d chemists in radius %f km",chemists.count, SEARCH_RADIUS_KM);
    
    
    [self.map showAnnotations:chemists animated:NO];
    chemistList=chemists;
//    [self.tableView reloadData];
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id < MKAnnotation >)annotation{
    static NSString *defaultPinID = @"com.invasivecode.pin";
    MKAnnotationView *av= (MKAnnotationView *)[self.map dequeueReusableAnnotationViewWithIdentifier:defaultPinID];
    if ( av == nil )
        av = [[MKPinAnnotationView alloc]
              initWithAnnotation:annotation reuseIdentifier:defaultPinID];
    Chemist *chemist=(Chemist *)annotation;
    
    OpeningStatus open= OpeningStatusUndefined;
    if([chemist respondsToSelector:@selector(getOpeningStatus)])
        open=[chemist getOpeningStatus];
    NSString *imageName=@"";
    switch(open){
        case OpeningStatusUndefined:
            imageName=@"puntatore";
            break;
        case OpeningStatusOpen:
            imageName=chemist.isParafarmacia ? @"punt_p_aperta" : @"punt_f_aperta";
            break;
        case OpeningStatusClosing:
            imageName=chemist.isParafarmacia ? @"punt_p_apertura" : @"punt_f_chiusura";
            break;
        case OpeningStatusClosed:
            imageName=chemist.isParafarmacia ? @"punt_p_chiusa" : @"punt_f_chiusa";
            break;
            
    }
    av.image=[UIImage imageNamed:imageName];
    CGRect frame=av.frame;
    frame.size=CGSizeMake(av.image.size.width/2, av.image.size.height/2);
    av.frame= frame;
    //    [annotationsArray addObject:av];
    return av;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
