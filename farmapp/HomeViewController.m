//
//  ViewController.m
//  farmapp
//
//  Created by Federico Ianne on 15/05/14.
//  Copyright (c) 2014 Iakta. All rights reserved.
//

#import "HomeViewController.h"
#import "DefUtilities.h"
#import "DetailViewController.h"
#import "DataManager.h"


#define TAB_CHEMIST 1
#define TAB_DRUGS 2
#define TAB_SICKNESS 3


@interface HomeViewController (){
//    NSArray *chemistList;
    NSArray *sicknessList;
    NSArray *drugList;
    int currentTab;
}

@end

@implementation HomeViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.btMedicine.titleLabel.font = [UIFont appFontOfSize:FONT_SIZE_SMALL];
    self.btPharmacy.titleLabel.font = [UIFont appFontOfSize:FONT_SIZE_SMALL];
    [self.tableView reloadData];
    chemistList=nil;
    sicknessList=nil;
    drugList=nil;
    currentTab=TAB_CHEMIST;
}

#pragma mark - map


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //center map on Turin
    self.map.region=MKCoordinateRegionMake(CLLocationCoordinate2DMake(45.066, 7.7),  MKCoordinateSpanMake(0.01, 0.01));
//    self.map.showsUserLocation=YES;
//    self.map.delegate=self;
//    map.centerCoordinate=CLLocationCoordinate2DMake(<#CLLocationDegrees latitude#>, <#CLLocationDegrees longitude#>)
    MKUserLocation *ul=[[MKUserLocation alloc]init];
    ul.coordinate=CLLocationCoordinate2DMake(45.066, 7.7);
    [self mapView:self.map didUpdateUserLocation:ul ];
    
    //
    sicknessList=[[DataManager instance]lookForSicknesses];
    drugList = [[DataManager instance] lookForDrugs];
}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation{
   /*// [self.map setCenterCoordinate: userLocation.coordinate animated:YES];
    //search
    NSArray *chemists=[[DataManager instance] searchChemistsWithName:nil andAddress:nil andCoordinate:userLocation.coordinate withinRange:SEARCH_RADIUS_KM];
    NSLog(@"Found %d chemists in radius %f km",chemists.count, SEARCH_RADIUS_KM);
    
    
    [self.map showAnnotations:chemists animated:NO];
    */
    [super mapView:mapView didUpdateUserLocation:userLocation];
//    chemistList=chemists;
    [self.tableView reloadData];
}
/*
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id < MKAnnotation >)annotation{
    static NSString *defaultPinID = @"com.invasivecode.pin";
    MKAnnotationView *av= (MKAnnotationView *)[self.map dequeueReusableAnnotationViewWithIdentifier:defaultPinID];
    if ( av == nil )
        av = [[MKPinAnnotationView alloc]
              initWithAnnotation:annotation reuseIdentifier:defaultPinID];
    Chemist *chemist=(Chemist *)annotation;
    
    OpeningStatus open= OpeningStatusUndefined;
    if([chemist respondsToSelector:@selector(getOpeningStatus)])
        open=[chemist getOpeningStatus];
    NSString *imageName=@"";
    switch(open){
        case OpeningStatusUndefined:
            imageName=@"puntatore";
            break;
        case OpeningStatusOpen:
            imageName=chemist.isParafarmacia ? @"punt_p_aperta" : @"punt_f_aperta";
            break;
        case OpeningStatusClosing:
            imageName=chemist.isParafarmacia ? @"punt_p_apertura" : @"punt_f_chiusura";
            break;
        case OpeningStatusClosed:
            imageName=chemist.isParafarmacia ? @"punt_p_chiusa" : @"punt_f_chiusa";
            break;
            
    }
    av.image=[UIImage imageNamed:imageName];
    CGRect frame=av.frame;
    frame.size=CGSizeMake(av.image.size.width/2, av.image.size.height/2);
    av.frame= frame;
//    [annotationsArray addObject:av];
    return av;
}
*/
- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view{
    id<MKAnnotation> ann=view.annotation;
    if([ann respondsToSelector:@selector(getOpeningStatus)]){
        Chemist *chemist=(Chemist *)ann;
        NSLog(@"Farmacia %@",chemist.denominazione);
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
        DetailViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"detailViewController"];
        [vc setChemist:chemist];
        [self.navigationController pushViewController:vc animated:YES];

    }
}
#pragma mark - actions

- (void)actionPharmacy {
    [self.btPharmacy setImage: [UIImage imageNamed:@"tab_farmacie.png"] forState:UIControlStateNormal];
    [self.btPharmacy setBackgroundImage: [UIImage imageNamed:@"tab_bg.png"] forState:UIControlStateNormal];
    [self.btPharmacy.titleLabel setTextColor:[UIColor lightGreenColor]];
    
    [self.btMedicine setImage: [UIImage imageNamed:@"tab_medicine.png"] forState:UIControlStateNormal];
    [self.btMedicine setBackgroundImage:nil forState:UIControlStateNormal];
    [self.btMedicine.titleLabel setTextColor:[UIColor darkGreenColor]];
    [self.btDiseases setImage: [UIImage imageNamed:@"tab_3.png"] forState:UIControlStateNormal];
    [self.btDiseases setBackgroundImage:nil forState:UIControlStateNormal];
    [self.btDiseases.titleLabel setTextColor:[UIColor darkGreenColor]];
    
    currentTab=TAB_CHEMIST;
    [self.tableView reloadData];
    
}

- (void)actionMedicine {
    [self.btMedicine setImage: [UIImage imageNamed:@"tab_medicine_hover.png"] forState:UIControlStateNormal];
    [self.btMedicine setBackgroundImage: [UIImage imageNamed:@"tab_bg.png"] forState:UIControlStateNormal];
    [self.btMedicine.titleLabel setTextColor:[UIColor lightGreenColor]];
    
    [self.btPharmacy setImage: [UIImage imageNamed:@"tab_farmacie_hover.png"] forState:UIControlStateNormal];
    [self.btPharmacy setBackgroundImage:nil forState:UIControlStateNormal];
    [self.btPharmacy.titleLabel setTextColor:[UIColor darkGreenColor]];
    [self.btDiseases setImage: [UIImage imageNamed:@"tab_3.png"] forState:UIControlStateNormal];
    [self.btDiseases setBackgroundImage:nil forState:UIControlStateNormal];
    [self.btDiseases.titleLabel setTextColor:[UIColor darkGreenColor]];
    currentTab=TAB_DRUGS;
        [self.tableView reloadData];
}

-(void)actionDiseases {
    [self.btDiseases setImage: [UIImage imageNamed:@"tab_3_hover.png"] forState:UIControlStateNormal];
    [self.btDiseases setBackgroundImage:[UIImage imageNamed:@"tab_bg.png"] forState:UIControlStateNormal];
    [self.btDiseases.titleLabel setTextColor:[UIColor lightGreenColor]];
    
    [self.btPharmacy setImage: [UIImage imageNamed:@"tab_farmacie_hover.png"] forState:UIControlStateNormal];
    [self.btPharmacy setBackgroundImage:nil forState:UIControlStateNormal];
    [self.btPharmacy.titleLabel setTextColor:[UIColor darkGreenColor]];
    [self.btMedicine setImage: [UIImage imageNamed:@"tab_medicine.png"] forState:UIControlStateNormal];
    [self.btMedicine setBackgroundImage:nil forState:UIControlStateNormal];
    [self.btMedicine.titleLabel setTextColor:[UIColor darkGreenColor]];
    currentTab=TAB_SICKNESS;
    [self.tableView reloadData];
}

#define CHEMIST_REUSE_ID @"chemistcell"
#define PARACHEMIST_REUSE_ID @"chemistcell"
#define DRUG_REUSE_ID @"drugcell"
#define SICKNESS_REUSE_ID @"sicknesscell"

#pragma mark - tableView delegate
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    if(currentTab==TAB_CHEMIST){
        Chemist *chemist=chemistList[indexPath.row];
    
        if(chemist.isParafarmacia){
            cell=[tableView dequeueReusableCellWithIdentifier:PARACHEMIST_REUSE_ID];
            if(!cell)
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:PARACHEMIST_REUSE_ID];
            cell.imageView.image=[UIImage imageNamed:  @"icona_parafarmacia" ];
        }
        else{
            cell=[tableView dequeueReusableCellWithIdentifier:CHEMIST_REUSE_ID];
            if(!cell)
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CHEMIST_REUSE_ID];
            cell.imageView.image=[UIImage imageNamed:   @"icona_farmacia"];
        }
        cell.textLabel.text = chemist.denominazione;
        cell.detailTextLabel.text=chemist.descrizione_indirizzo;
        cell.textLabel.font = [UIFont appFontOfSize:FONT_SIZE_SMALL];
        cell.textLabel.textColor = [UIColor lightGreenColor];
    
        cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icona_dettaglio.png"]];
    }else if(currentTab==TAB_DRUGS){
        NSManagedObject *drugOrIngredient=drugList[indexPath.row];
        
        cell=[tableView dequeueReusableCellWithIdentifier:DRUG_REUSE_ID];
        if(!cell)
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:DRUG_REUSE_ID];
        
        if([drugOrIngredient isKindOfClass:[Drug class]]){
            Drug *drug=(Drug*)drugOrIngredient;
            
            cell.imageView.image=[UIImage imageNamed:  @"tab_medicine" ];
            cell.textLabel.text = drug.name;
            cell.detailTextLabel.text=drug.detail;
        }
        else{
            ActiveIngredient *ingredient=(ActiveIngredient *)drugOrIngredient;
            cell.imageView.image=[UIImage imageNamed:   @"tab_medicine_hover"];
            cell.textLabel.text = ingredient.name;
            cell.detailTextLabel.text=ingredient.detail;
        }
        
        cell.textLabel.font = [UIFont appFontOfSize:FONT_SIZE_SMALL];
        cell.textLabel.textColor = [UIColor lightGreenColor];
        
        cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icona_dettaglio.png"]];
    }else if(currentTab==TAB_SICKNESS){
        Sickness *sickness=sicknessList[indexPath.row];
        cell=[tableView dequeueReusableCellWithIdentifier:SICKNESS_REUSE_ID];
        if(!cell)
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:SICKNESS_REUSE_ID];
        
        if(sickness.icon){
            
            cell.imageView.image=[UIImage imageWithData:sickness.image];
        }
        else{
            cell.imageView.image=[UIImage imageNamed:@"tab_3"];
        }
        
        cell.textLabel.text = sickness.name;
        cell.detailTextLabel.text=sickness.detail;
        cell.textLabel.font = [UIFont appFontOfSize:FONT_SIZE_SMALL];
        cell.textLabel.textColor = [UIColor lightGreenColor];
        
//        cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icona_dettaglio.png"]];
    }
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (currentTab) {
        case TAB_CHEMIST:
            return chemistList.count;
        case TAB_DRUGS:
            return drugList.count;
        case TAB_SICKNESS:
            return sicknessList.count;
            
        default:
            return 0;
    }
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    switch(currentTab){
        case TAB_CHEMIST:{
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
            DetailViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"detailViewController"];
            [vc setChemist:(Chemist *)chemistList[indexPath.row]];
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        case TAB_DRUGS:{
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
            DetailViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"detailViewController"];
            [vc setDrug:(Drug *)drugList[indexPath.row]];
            
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        case TAB_SICKNESS:{
            //switch to
            Sickness *sick=sicknessList[indexPath.row];
            //switch to medicinals
            NSMutableArray *drugs=[NSMutableArray array];
            for(Drug *d in sick.sickness_drug){
                [drugs addObject:d];
            }
            drugList=drugs;
            [self actionMedicine];
        }
            break;
    }
    
}

@end
